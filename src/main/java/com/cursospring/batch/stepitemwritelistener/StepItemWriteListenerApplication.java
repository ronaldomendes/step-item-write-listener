package com.cursospring.batch.stepitemwritelistener;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.stepitemwritelistener"})
public class StepItemWriteListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StepItemWriteListenerApplication.class, args);
    }

}
