package com.cursospring.batch.stepitemwritelistener.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

@Slf4j
public class DemoJobListener extends JobExecutionListenerSupport {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("Before {} execution", jobExecution.getJobInstance().getJobName());
        jobExecution.getExecutionContext().putString("beforeJob", "beforeValue");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("After {} execution. The value of \"beforeJob\" key is: {}",
                jobExecution.getJobInstance().getJobName(),
                jobExecution.getExecutionContext().getString("beforeJob"));

        if (jobExecution.getStatus().equals(BatchStatus.COMPLETED)) {
            log.info("Job successfully completed");
        } else {
            log.error("Job failed: {}", jobExecution.getAllFailureExceptions().toString());
        }
    }
}
