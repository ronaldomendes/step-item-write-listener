package com.cursospring.batch.stepitemwritelistener.listener;

import com.cursospring.batch.stepitemwritelistener.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

@Slf4j
public class WriterListener implements ItemWriteListener<Employee> {

    @Override
    public void beforeWrite(List<? extends Employee> employees) {
        employees.forEach(e -> log.info("Before write process: {}", e.toString()));
    }

    @Override
    public void afterWrite(List<? extends Employee> employees) {
        employees.forEach(e -> log.info("After write process: {}", e.toString()));
    }

    @Override
    public void onWriteError(Exception e, List<? extends Employee> employees) {
        log.error("On write error: {} ", e.getMessage());
    }
}
