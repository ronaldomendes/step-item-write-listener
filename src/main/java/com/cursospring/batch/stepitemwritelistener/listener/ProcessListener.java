package com.cursospring.batch.stepitemwritelistener.listener;

import com.cursospring.batch.stepitemwritelistener.dto.EmployeeDTO;
import com.cursospring.batch.stepitemwritelistener.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;

@Slf4j
public class ProcessListener implements ItemProcessListener<EmployeeDTO, Employee> {

    @Override
    public void beforeProcess(EmployeeDTO dto) {
        log.info("Before process: {}", dto.toString());
    }

    @Override
    public void afterProcess(EmployeeDTO dto, Employee employee) {
        log.info("After process: {}", employee.toString());
    }

    @Override
    public void onProcessError(EmployeeDTO dto, Exception e) {
        log.error("On process error: {} ", e.getMessage());
    }
}
